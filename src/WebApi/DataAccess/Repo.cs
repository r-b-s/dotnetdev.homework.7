
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.DataAccess
{
    public class Repo: IRepo
    {
        private readonly List<Customer> _customers;

        public Repo()
        {
            _customers = new List<Customer>();
        }

        public Task<long> NewCustomer(Customer customer)
        {
            long id = _customers.Count>0? _customers.Max(e => e.Id) + 1:1;
            _customers.Add(new Customer
            {
                Firstname= customer.Firstname,
                Lastname= customer.Lastname,
                Id=id
            });
            return Task.FromResult(id);
        }

        public Task<Customer> GetCustomer(long id)
        {
            return Task.FromResult(_customers.FirstOrDefault(e=>e.Id==id));
        }
    }
}
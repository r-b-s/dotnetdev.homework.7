using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApi.DataAccess;
using static System.Net.Mime.MediaTypeNames;

namespace WebApi
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void Configure(IApplicationBuilder application, IWebHostEnvironment env)
        {
            application.UseHealthChecks("/health");
            application.UseExceptionHandler("/Error");           
            application.UseRouting();
            application.UseEndpoints(e => e.MapControllers());            
            application.UseSwagger();
            application.UseSwaggerUI();
            
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();
            services.AddSingleton<IRepo, Repo>();

            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();
        }
    }
}
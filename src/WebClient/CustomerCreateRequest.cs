using System.Text.Json;

namespace WebClient
{
    public class CustomerCreateRequest
    {
        public CustomerCreateRequest()
        {
        }

        public CustomerCreateRequest(
            string firstName,
            string lastName)
        {
            Firstname = firstName;
            Lastname = lastName;
            json = JsonSerializer.Serialize(new Customer {Id=0,Firstname=firstName,Lastname=lastName });
        }

        public string Firstname { get; init; }

        public string Lastname { get; init; }

        public string json { get; init; }
    }
}
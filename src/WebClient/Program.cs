﻿using System;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        private static HttpClient client = new HttpClient();
        static Task Main(string[] args)
        {
            
            Console.WriteLine("Commnds:");
            Console.WriteLine("exit");
            Console.WriteLine("{id}");
            Console.WriteLine("new");

            string cmd = "";

            while (cmd != "exit")
            {
                cmd = Console.ReadLine();
                if (int.TryParse(cmd, out int id))
                {
                    getCustomer(id.ToString());
                }
                if (cmd== "new")
                {
                    var newCustomer = RandomCustomer();
                    var httpContent = new StringContent(newCustomer.json, Encoding.UTF8, "application/json");

                    var httpResponse = client.PostAsync("http://localhost:5000/customers", httpContent);

                    // If the response contains content we want to read it!
                    if (httpResponse.Result.StatusCode == HttpStatusCode.OK)
                    {
                        var responseContent =  httpResponse.Result.Content.ReadAsStringAsync();
                        getCustomer(responseContent.Result);
                    }
                }
            }

            client.Dispose();
            return Task.CompletedTask;

        }

         static void getCustomer(string id)
        {
        
            var httpResponse = client.GetAsync("http://localhost:5000/customers/"+id);

            // If the response contains content we want to read it!
            if (httpResponse.Result.StatusCode == HttpStatusCode.OK)
            {
                var responseContent = httpResponse.Result.Content.ReadAsStringAsync();

                Console.WriteLine( responseContent.Result);
                return;
            }
            Console.WriteLine("Not defined");
        }
    

         static CustomerCreateRequest RandomCustomer()
        {
            var httpResponse = client.GetAsync("https://names.drycodes.com/10?nameOptions=girl_names");

            // If the response contains content we want to read it!
            if (httpResponse.Result.StatusCode == HttpStatusCode.OK)
            {
                var responseContent = httpResponse.Result.Content.ReadAsStringAsync();
                var names=responseContent.Result.Replace(",", " ").Replace("_", " ").Replace($"\"", "").Replace("[", "").Split(" ");

                return new CustomerCreateRequest(names[0], names[1]);
            }
            return new CustomerCreateRequest("notRandom", "notRandom");
            }
        }
    }
